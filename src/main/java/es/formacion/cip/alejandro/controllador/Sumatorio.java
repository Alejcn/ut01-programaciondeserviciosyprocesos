package main.java.es.formacion.cip.alejandro.controllador;

import java.util.List;

public class Sumatorio {
	
	public int sumar(int numero1, int numero2) {
		int sumaTotal = 0;
		int numeroMenor = Math.min(numero1, numero2);
		int numeroMayor = Math.max(numero1, numero2);
		
		for(int i = numeroMenor; i <= numeroMayor; i++) {
			sumaTotal += i;
		}
		return sumaTotal;
	}
}

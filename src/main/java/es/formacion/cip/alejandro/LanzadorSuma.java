package main.java.es.formacion.cip.alejandro;

import main.java.es.formacion.cip.alejandro.controllador.Sumatorio;

public class LanzadorSuma {
	
	public void ejecutar(String num1, String num2){
		Sumatorio sumatorio = new Sumatorio();
		ProcessBuilder pb = new ProcessBuilder(sumatorio.sumar(num1, num2));
		try {
			pb.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		LanzadorSuma proceso = new LanzadorSuma();
		proceso.ejecutar(1, 30);
		proceso.ejecutar(70, 100);
	}
}
